<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<main class="single-event">
		<section class="container default-page">
				<h1 class="section-title text-center"><?php the_title(); ?></h1>
				<h4 class="event-date text-center"><?php the_date('m/d/Y'); ?></h4>
        	</div>
			<?php if(has_post_thumbnail()): ?>
				<section style="background:url('<?php the_post_thumbnail_url(); ?>') no-repeat center / cover;" class="featured-image-section">
				</section>
			<?php endif; ?>	

            <div class="content clearfix" >
				<div class="col-md-8 col-md-offset-2"><?php the_content(); ?></div></div>

			<div class="text-center">
				<h1 class="section-title"><?php echo __('[:fr]NOUVELLES ET EVÈNEMENTS[:en]NEWS AND EVENTS'); ?></h1>
			</div>
            <div class="row news-row"><?php 
        	$args = array(
                'post_type'=>'post',
                'posts_per_page' => -1,
                'showposts' => '3',
                'category_name' => 'news,events',
            );

            $the_query = new WP_Query( $args );
            the_post();
            while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                	<div class="col-md-4">
                		<div class="news-box">
	                		<a class="absolute" href="<?php the_permalink(); ?>"></a>
	                		<?php  if(has_post_thumbnail()): ?>
		                		<div class="event-thumbnail" style="background:url('<?php the_post_thumbnail_url("full"); ?>')  no-repeat center top / cover; "></div>
	                		<?php endif;?>
	                        <div class="news-box-content">
		                		<div class="news-date">
		                            <p><b><?php echo get_the_date('j'); echo " "; echo get_the_date('M'); ?></b></p>
		                            <p><?php echo get_the_date('Y'); ?></p>
		                        </div>
                                <div class="event-content">
                                    <h2 class="event-title"><?php the_title(); ?></h2>
                                    <?php echo my_excerpts(15); ?>
                                    <a class="read-more fr" href="<?php the_permalink(); ?>"><?php echo __('[:fr]Lire La Suite[:en]Read More'); ?> >></a>
                                </div>
		                        	
	                        </div>
                    	</div>
                	</div>
                <?php endwhile; ?>
            </div>
            <div class="event-page-link"><a href="<?php echo get_home_url()?>/category/news/"><p><?php echo __('[:fr]Plus[:en]More'); ?> >></p></a></div>
		</section>
		
	</main>
<?php endwhile; endif; wp_reset_query(); wp_reset_postdata();?>	
<?php get_footer(); ?>