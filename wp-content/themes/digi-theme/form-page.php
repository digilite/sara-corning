<?php /* Template Name: Page with Form */ ?>
<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<main  style="background:url('<?php echo bloginfo('template_url'); ?>/img/page_background.svg') no-repeat left bottom; position: relative;" class="form-page">
		<?php if(has_post_thumbnail()): ?>
			<div style="background:url('<?php the_post_thumbnail_url(); ?>') no-repeat center / cover;" class="featured-image-section">
			</div>
		<?php else: ?>
			<div style="background:url('<?php echo bloginfo('template_url'); ?>/img/img-2051_orig.svg') no-repeat center / cover;" class="featured-image-section">
				<div class="featured-image-overlay"></div>
			</div>
		<?php endif; ?>	

		<section class="default-page">
			<h1 class="section-title text-center"><?php the_title(); ?></h1>

			<div class="row">
				<div class="col-md-6"><div class="form-text">
            		<div class="content" ><?php the_field('form_page_text'); ?></div>
            	</div>
            	</div>
				<div class="col-md-6 form-border">
					<div class="form-shortcode"><?php echo do_shortcode(get_field('form_shortcode')); ?>
					</div>
				</div>
			</div>
            
		</section>
		
	</main>

<?php endwhile; endif; ?>	
</main>

<?php get_footer(); ?>