<?php get_header(); ?>
    <main class="front-page bg-image" style="background:url('<?php echo bloginfo('template_url'); ?>/img/sara_logo.svg')  no-repeat right bottom; ">
        <section class="slider-section">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php while(have_rows("home_slider")): the_row(); ?>
                        <?php if(get_sub_field("image")): ?>
                            <div class="swiper-slide" style="background-image:url(<?php the_sub_field('image')?>)"></div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </div>
             <div class="swiper-pagination"></div>
            </div>
        </section>

        <section class="our-mission padding-30">
            <div class="container">
                <?php if(get_field('section_title')):?>
                    <h1 class="section-title text-center"><?php the_field('section_title'); ?></h1>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <?php if(get_field('section_text')):?>
                            <div class="mission-text"><?php the_field('section_text'); ?></div>
                        <?php endif; ?>
                        <?php  if(get_field('link_url') || get_field('link_text')): ?>
                            <a class="read-more" href="<?php the_field('link_url'); ?>"><?php the_field('link_text'); ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="for-educators padding-30" id="for-educators">
            <div class="container">
                <div class="clearfix educators-header">
                    <h1 class="section-title text-uppercase fl"><?php echo __('[:fr]Pour Les Éducateurs[:en]For Educators'); ?> </h1>
                    <a class="page-link fr" href="<?php echo get_home_url()?>/courses"><?php echo __('[:fr]Plus Sur Notre Portail[:en]More on Our Portal'); ?> >></a>
                </div>
                <div class="row courses-row">
                    <?php $args = array(
                    'post_type'=>'sfwd-courses',
                    'posts_per_page' => -1,
                    'showposts' => '4',
                );

                $the_query = new WP_Query( $args );
                the_post();
                while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="col-md-3 col-xs-6">
                        <div class="news-box ">
                            <div class="event-thumbnail thumbnail-img" style="background:url('<?php the_post_thumbnail_url("full"); ?>')  no-repeat center top / cover; ">
                                <a class="absolute" href="<?php the_permalink(); ?>"></a>
                                <p class="article-title"><?php the_title(); ?></p>
                                <a class="download-icon" href="<?php the_permalink(); ?>" download><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" width="1.15em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 896"><path d="M763 196q-34-91-106.5-142T478 3Q341 3 251.5 92.5T153 318q-67 22-110 83.5T0 540q0 94 64 163t152 69h28q7 0 13-2.5t10-7 6.5-10.5 2.5-12q0-13-9-22.5t-23-9.5h-28q-62 0-107.5-50T63 540q0-60 37-108t90-57l29-5-2-30h-1q0-119 72-196t190-77q179 0 231 169l7 22 23 1q59 1 110 30t82 79.5T962 477q0 28-6 55.5t-16.5 53-27 48-36 39-44.5 26-52 9.5h-9q-6 0-12 2.5t-10.5 7-7 10T739 740q0 13 9.5 22.5T771 772q58-2 107-28.5t80-69 48.5-93.5 17.5-104q0-109-75.5-189T763 196zM646 711q-6-7-15.5-7t-16.5 7l-70 78V467q0-3-1-6.5t-2-6-3-5.5-4-5-4.5-3.5-5.5-3-6-2-6-.5q-14 0-23 9.5t-9 22.5v322l-68-76q-7-7-16.5-7.5T379 711l-8 5q-7 7-7 17t7 17l116 127v1l8 8q7 7 16.5 7t16.5-7l8-8v-1l118-129q15-15 0-30z" fill="#fff"/></svg></a>
                            </div>
                            
                        </div>
                    </div>
                <?php endwhile; ?>
                </div>
                    
            </div>
        </section>

        <section class="news-events padding-30" id="news-events">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                    <h1 class="section-title text-uppercase"><?php echo __('[:fr]Nouvelles[:en]News'); ?></h1>
                        <div class="row">
                            <?php $args = array(
                                'post_type'=>'post',
                                'posts_per_page' => -1,
                                'showposts' => '3',
                                'category_name' => 'news',
                            );

                            $the_query = new WP_Query( $args );
                            the_post();

                            $i = 0;
                            while ( $the_query->have_posts() ) : $i ++;
                                $the_query->the_post(); ?>

                                <?php if($i === 1): ?>
                                    <div class="col-md-12">
                                        <div class="event-box full-width">
                                            <a class="absolute" href="<?php the_permalink(); ?>"></a>
                                            <div class="event-info">
                                                <div class="event-content clearfix">
                                                    <h2 class="event-title"><?php the_title(); ?></h2>
                                                    <p class="event-date"><?php the_date('m/d/Y'); ?></p>
                                                    <?php echo my_excerpts(35); ?>
                                                    <a class="read-more " href="<?php the_permalink(); ?>"><?php echo __('[:fr]Lire La Suite[:en]Read More'); ?>>></a>
                                                </div>
                                                <div class="event-thumbnail" style="background:url('<?php the_post_thumbnail_url("medium"); ?>')  no-repeat center top / cover; "></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row event-row-bottom">
                                <?php else: ?>
                                    <div class="col-md-6">
                                        <div class="event-box half-width">
                                            <a class="absolute" href="<?php the_permalink(); ?>"></a>
                                            <div class="event-thumbnail" style="background:url('<?php the_post_thumbnail_url("full"); ?>')  no-repeat center top / cover; "></div>
                                            <h2 class="event-title"><?php the_title(); ?></h2>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            <?php endwhile;?>
                            </div>
                        </div>
                    <div class="col-md-6">
                        <h1 class="section-title text-uppercase"><?php echo __('[:fr]Evènements[:en]Events'); ?></h1>

                        <div class="news-section">
                            <?php $args = array(
                                'post_type'=>'post',
                                'posts_per_page' => -1,
                                'category_name' => 'events',
                            );

                            $the_query = new WP_Query( $args );
                            the_post();
                            while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <div class="news-item">
                                    <a class="absolute" href="<?php the_permalink(); ?>"></a>
                                    <div class="circle"></div>
                                    <div class="news-date">
                                        <p><?php echo get_the_date('j'); ?></p>
                                        <p><b><?php echo get_the_date('M'); ?></b></p>
                                    </div>
                                    <div class="verticale-line"></div>
                                    <div class="news-info clearfix">
                                        <h3 class="news-title"><?php the_title(); ?></h3>
                                        <div class="news-content"><?php echo my_excerpts(20); ?></div>
                                        <a class="read-more fr" href="<?php the_permalink(); ?>"><?php echo __('[:fr]Lire La Suite[:en]Read More'); ?> >></a>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    </main>
<?php get_footer(); ?>