<?php if(get_field('location_address', 'option') || get_field('email_address', 'option')): ?>
    <ul class="email-and-location">
        <li class="address">
            <a href="#"><i class="material-icons">location_on</i></a>
            <span><?php the_field('location_address', 'option'); ?></span>
        </li>

        <li class="email">
            <a href="mailto:<?php the_field('email_address', 'option'); ?>"><i class="material-icons">email</i></a>
            <span><?php the_field('email_address', 'option'); ?></span>
        </li>
    </ul>
<?php endif; ?>
