<?php get_header(); ?>

<?php if (have_posts()) : $i=0; ?>
	<main class="news-page container">
		<h1 class="section-title text-center">Lesson Plans</h1>
		<div class="row">

		<?php while (have_posts()) : the_post(); ?>

		    		<div class="col-md-12 big-post">
		    			<div class="news-box">
                		<a class="absolute" href="<?php the_permalink(); ?>"></a>
                        <div class="news-box-content">
                            <div class="event-content courses">
                                <h2 class="event-title"><?php the_title(); ?></h2>
                                <a class="read-more fr" href="<?php the_permalink(); ?>">Go To Course >></a>
                            </div>
	                        	
                        </div>
                	</div>
	    		</div>	
		<div class="nav-previous alignleft"><?php previous_posts_link( 'Newer Posts' ); ?></div>
		<div class="nav-next alignright"><?php next_posts_link( 'Older Posts' ); ?></div>
	    <?php endwhile; endif; wp_reset_query(); wp_reset_postdata();?>
	    </div>
	</main>
	

<?php get_footer(); ?>