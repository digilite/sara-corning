<?php get_header(); ?>
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <main id="contact-us-page">

        <section class="map-section">
            <div id="google-map" class="map"></div>
        </section>

        <section class="address-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-2">
                        <?php get_template_part("templates/theme", "settings"); ?>
                    </div>

                    <div class="social-icons co-ld-1">
                        <ul class="footerIcons">
                            <?php get_template_part("social"); ?>
                            <li>
                                <a class="site-social-mail" href="mailto:info@corningcentre.org" target="_blank" alt-text="Mail">
                                    <i class="far fa-envelope"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="contact-form-section padding-30">
            <div class="container">
                <h1 class="text-center section-title"><?php the_title(); ?></h1>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="contact-form">
                            <?php echo do_shortcode("[formidable id=6]"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <?php endwhile; endif; ?>
<?php get_footer(); ?>