<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<main class="single-event">
		<section class="container default-page">
				<h1 class="section-title text-center"><?php the_title(); ?></h1>
				<h4 class="event-date text-center"><?php the_date('m/d/Y'); ?></h4>
        	
			<?php if(has_post_thumbnail()): ?>
				<section style="background:url('<?php the_post_thumbnail_url(); ?>') no-repeat center / cover;" class="featured-image-section">
				</section>
			<?php endif; ?>	

            <div class="content clearfix" ><?php the_content(); ?></div>

			<div class="text-center">
				<h1 class="section-title">RELATED COURSES</h1>
			</div>
		</section>
		
        <section class="for-educators">
            <div class="container">
                <div class="row courses-row">
                    <?php $args = array(
                    'post_type'=>'sfwd-courses',
                    'posts_per_page' => -1,
                    'showposts' => '4',
                );

                $the_query = new WP_Query( $args );
                the_post();
                while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="col-md-3 col-xs-6">
                        <div>
                            <div class="event-thumbnail thumbnail-img" style="background:url('<?php the_post_thumbnail_url("full"); ?>')  no-repeat center top / cover; ">
                                <a class="absolute" href="<?php the_permalink(); ?>"></a>
                                <p class="article-title"><?php the_title(); ?></p>
                                <a class="download-icon" href="<?php the_permalink(); ?>" download><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" width="1.15em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 896"><path d="M763 196q-34-91-106.5-142T478 3Q341 3 251.5 92.5T153 318q-67 22-110 83.5T0 540q0 94 64 163t152 69h28q7 0 13-2.5t10-7 6.5-10.5 2.5-12q0-13-9-22.5t-23-9.5h-28q-62 0-107.5-50T63 540q0-60 37-108t90-57l29-5-2-30h-1q0-119 72-196t190-77q179 0 231 169l7 22 23 1q59 1 110 30t82 79.5T962 477q0 28-6 55.5t-16.5 53-27 48-36 39-44.5 26-52 9.5h-9q-6 0-12 2.5t-10.5 7-7 10T739 740q0 13 9.5 22.5T771 772q58-2 107-28.5t80-69 48.5-93.5 17.5-104q0-109-75.5-189T763 196zM646 711q-6-7-15.5-7t-16.5 7l-70 78V467q0-3-1-6.5t-2-6-3-5.5-4-5-4.5-3.5-5.5-3-6-2-6-.5q-14 0-23 9.5t-9 22.5v322l-68-76q-7-7-16.5-7.5T379 711l-8 5q-7 7-7 17t7 17l116 127v1l8 8q7 7 16.5 7t16.5-7l8-8v-1l118-129q15-15 0-30z" fill="#fff"/></svg></a>
                            </div>
                            
                        </div>
                    </div>
                <?php endwhile; ?>
                </div>
                    
            </div>
        </section>
            <div class="event-page-link"><a href="<?php echo get_home_url()?>/courses"><p>More >></p></a></div>
		
	</main>
<?php endwhile; endif; wp_reset_query(); wp_reset_postdata();?>	
<?php get_footer(); ?>