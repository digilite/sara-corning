<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<!-- Generate favicon here http://www.favicon-generator.org/ -->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header>
		<div>
			<nav class="navbar-collapse collapse" id="navigation-bar">
                <a class="navbar-brand" href="<?php bloginfo('url') ?>">
                    <img class="img-responsive" alt="" src="<?php echo bloginfo('template_url'); ?>/img/Logo.svg"/>
                </a>
                <div class="container menu-container">
				<?php wp_nav_menu(array('theme_location' => 'main-menu', 'menu'=>'Top Navigation', 'container' => '', 'menu_class' =>'nav navbar-nav navbar-right nav-menu', 'menu_id' => 'main-menu-desktop', 'walker'	 => new BootstrapNavMenuWalker() ));?>
				<?php if ( function_exists ( 'wpm_language_switcher' ) ) wpm_language_switcher ('list', "flag");?>
					
				</div>
				<?php get_search_form()?>
		        <div class="toggle-button"><i class="fa fa-bars"></i></div>
		        <div class="close-button"><i class="fa fa-times-thin fa-2x"></i></div>
			</nav>
		</div>

	</header>