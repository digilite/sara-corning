<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<main  style="background:url('<?php echo bloginfo('template_url'); ?>/img/page_background.svg') no-repeat left bottom; position: relative;">
		<?php if(has_post_thumbnail()): ?>
			<section style="background:url('<?php the_post_thumbnail_url(); ?>') no-repeat center / cover;" class="featured-image-section">
			</section>
		<?php endif; ?>	

		<section class="container default-page">
			<h1 class="text-center section-title"><?php the_title(); ?></h1>	
			
            <div class="content row" >
			<div class="col-md-8 col-md-offset-2"><?php the_content(); ?></div></div>
            
		</section>
            <div class="form-border"></div>
		
	</main>

<?php endwhile; endif; ?>	
</main>

<?php get_footer(); ?>