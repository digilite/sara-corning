<?php 
	get_header(); 
	$current_quesied_object = get_queried_object();

?>
<?php if (have_posts()) : $i=0; ?>
	<main class="news-page container">
		<h1 class="section-title text-center"><?php single_cat_title();?></h1>
		<?php while (have_posts()) : the_post(); ?>

				<div class="row">
		    		<div class="col-md-12 big-post">
		    			<div class="news-box">
                		<a class="absolute" href="<?php the_permalink(); ?>"></a>
                		<?php  if(has_post_thumbnail()): ?>
                			<div class="event-thumbnail" style="background:url('<?php the_post_thumbnail_url("full"); ?>')  no-repeat center top / cover; "></div>
						<?php endif; ?>
                        <div class="news-box-content">
							<?php if ( $current_quesied_object->term_id == 1 ): ?>
								<div class="news-date">
									<p><b><?php echo get_the_date('j'); echo " "; echo get_the_date('M'); ?></b></p>
									<p><?php echo get_the_date('Y'); ?></p>
								</div>
							<?php endif; ?>
                            <div class="event-content">
                                <h2 class="event-title"><?php the_title(); ?></h2>
                                <p><?php echo my_excerpts(50); ?></p>
                                <a class="read-more fr" href="<?php the_permalink(); ?>"><?php echo __('[:fr]Lire La Suite[:en]Read More'); ?> >></a>
                            </div>
	                        	
                        </div>
                	</div>
	    		</div>
	    	</div>	
		<div class="nav-previous alignleft"><?php previous_posts_link( 'Newer Posts' ); ?></div>
		<div class="nav-next alignright"><?php next_posts_link( 'Older Posts' ); ?></div>
	    <?php endwhile; endif; wp_reset_query(); wp_reset_postdata();?>
	</main>
	

<?php get_footer(); ?>