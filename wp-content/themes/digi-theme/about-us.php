<?php /* Template Name: Our Mission */ ?>
<?php get_header(); ?>

<main class="our-mission-page">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<section class="our-mission container" id="our-mission">
		<div class="row"><div class="col-md-8 col-md-offset-2"> 
			<h1 class="text-center section-title"><?php the_title(); ?></h1>
			<div class="our-mission-content"><?php the_content(); ?></div>
		</div></div>
		
	</section>

	<section class="biography container" id="biography">
		<h1 class="section-title text-center"><?php the_field('bio_field_title')?></h1>
		<div class="row">
			<div class="col-md-4">
				<div class="image-column">
					<?php if( have_rows('bio_images_column') ): ?>
						<ul class="list-unstyled">
					    	<?php while ( have_rows('bio_images_column') ) : the_row(); ?>
						        	<li><img src="<?php the_sub_field('bio_image'); ?>" ></li>
					        <?php endwhile; ?>
			        	</ul>
					<?php else :

					endif; ?>
				</div>
			</div>
			<div class="col-md-8">
				<?php if( have_rows('bio_images_column') ): $i=0;?>
					<div class="biography-column">
					    	<?php while ( have_rows('biography_column') ) :  the_row(); ?>
					    		<?php $i ++; if($i%2===1): ?>
						    		<div class="bio-event-odd">
						    			<div class="border-col text-col"><?php the_sub_field('event_text'); ?></div>
						    			<div class="bio-date">
						    				<?php
					    					$string = get_sub_field('event_year');
					    					$array = explode(',', $string);
					    					foreach($array as $value) {
					    						?><span><?php 
					    						echo $value;
					    						?></span><?php 
					    					}?>
					    				</div>
						    		</div>
					    		<?php else: ?>
						    		<div class="bio-event-even">
						    			<div class="bio-date border-col">
						    				<?php
					    					$string = get_sub_field('event_year');
					    					$array = explode(',', $string);
					    					foreach($array as $value) {
					    						?><span><?php 
					    						echo $value;
					    						?></span><?php 
					    					}?>
					    				</div>
						    			<div class="text-col"><?php the_sub_field('event_text'); ?></div>
						    		</div>
					    		<?php endif; ?>
						        	
						        	
					        <?php endwhile; ?>
					    </div>
					<?php else :

					endif; ?>
			</div>
		</div>
		
	</section>

	<section class="mission-images" id="mission-images">
        <div class="swiper-container" id="mission-slider">
            <div class="swiper-wrapper">
                <?php while(have_rows("our_mission_images")): the_row(); ?>
                    <?php if(get_sub_field("our_mission_image")): ?>
                        <div class="swiper-slide" style="background-image:url(<?php the_sub_field('our_mission_image')?>)"></div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
             <div class="swiper-pagination"></div>
        </div>
	</section>

<?php endwhile; endif; ?>	
</main>
<?php get_footer(); ?>