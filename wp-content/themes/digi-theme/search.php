<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<!-- Generate favicon here http://www.favicon-generator.org/ -->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header>
		<div class="search-header">
			<nav class="navbar-collapse collapse" id="navigation-bar">
                <a class="navbar-brand" href="<?php bloginfo('url') ?>">
                    <img class="img-responsive" alt="" src="<?php echo bloginfo('template_url'); ?>/img/Sara Logo.svg"/>
                </a>
                <div class="container menu-container">
				<?php wp_nav_menu(array('theme_location' => 'main-menu', 'menu'=>'Top Navigation', 'container' => '', 'menu_class' =>'nav navbar-nav navbar-right nav-menu', 'menu_id' => 'main-menu-desktop', 'walker'	 => new BootstrapNavMenuWalker() ));?>
					
				</div>
				<?php get_search_form()?>
		        <div class="toggle-button"><i class="fa fa-bars"></i></div>
			</nav>
		</div>

	</header>
<main class="container">
	<h1 class="section-title text-center">Search Results:</h1>
	<?php
	    global $query_string;
	    $query_args = explode("&", $query_string);
	    $search_query = array();

	    foreach($query_args as $key => $string) {
	      $query_split = explode("=", $string);
	      $search_query[$query_split[0]] = urldecode($query_split[1]);
	    } 
	    $the_query = new WP_Query($search_query);
	    if ( $the_query->have_posts() ) : 
	    ?>

	    <ul class="search-list">    
	    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	        <li class="search-list-item">
	            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	            <p><?php echo the_excerpt(); ?></p>
	        </li>   
	    <?php endwhile; ?>
	    </ul>

	    <?php wp_reset_postdata(); ?>

	<?php else : ?>
	    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>
</main>
<?php get_footer(); ?>