		<footer>
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 col-sm-6">
                            <?php wp_nav_menu(array('theme_location' => 'footer-menu-1', 'menu'=>'Footer Menu 1', 'container' => '', 'menu_class' =>'footer-menu', 'walker'	 => new BootstrapNavMenuWalker() ));?>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <?php wp_nav_menu(array('theme_location' => 'footer-menu-2', 'menu'=>'Footer Menu 2', 'container' => '', 'menu_class' =>'footer-menu', 'walker'	 => new BootstrapNavMenuWalker() ));?>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <?php get_template_part("templates/theme", "settings"); ?>
                        </div>
                        <div class="col-md-offset-1 col-md-3 col-sm-6">
                            <div class="subscribe-form">
                                <span class="subscribe-form-title"><?php echo __('[:fr]Abonnez-Vous à Notre Bulletin Électronique[:en]Subscribe to Our E-Newsletter'); ?></span>
                                <?php echo do_shortcode('[formidable id=5]'); ?>
                            </div>
                            <div class="social-icons">
                                <ul class="footerIcons">
                                    <?php get_template_part("social"); ?>
                                    <li>
                                        <a class="site-social-mail" href="mailto:info@corningcentre.org" target="_blank" alt-text="Mail">
                                            <i class="far fa-envelope"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container footer-half-container">
                        <div class="footer-half">
                            <p class="footer-text text-left">&copy;2013- <?php echo date("Y")." | ".get_bloginfo("name"); ?><?php echo __("[:fr]Centre Pour L'éducation au Génocide[:en]Centre for Genocide Education"); ?></p>
                        </div>
                        <div class="footer-half">
                            <p class="footer-text text-right"><?php echo __("[:fr]Site Développé Par [:en]Website Developed by "); ?><a href="https://digilite.ca/"> Digilite</a></p>
                        </div>
                </div>
            </div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>