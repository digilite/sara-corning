var $ = jQuery;


$(document).ready(function() {
    $("li.current-menu-item a").click(function(event){
        if (this.hash != "") {
            event.preventDefault();
            var target = $(this.hash);
            $('html').animate({
                scrollTop: target.offset().top - 10
            }, 500);
        }
    });


    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.col-md-4'
      });

    $("iframe[src*='youtube.com']").each(function(){
        $(this).wrap('<div class="embed-responsive embed-responsive-16by9"></div>');
    });

    $("li:not(.current_menu_item) a").click(function(event){
        if (this.hash != "") {
            event.preventDefault();
            window.location.href = this.href;
        }
    });
    if($(window).width() < 768){
        var swiper = new Swiper('.bg-image .swiper-container', {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            simulateTouch: true,
            slideToClickedSlide: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
              },
            slidesPerView: 1,
            loop: true,
            coverflowEffect: {
                rotate: 0,
                stretch: 0,
                depth: 400,
                modifier: 1,
                slideShadows : true,
            }
        });
        var missionSwiper = new Swiper('.mission-images .swiper-container#mission-slider', {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
              },
            slidesPerView: 1,
            loop: true,
            coverflowEffect: {
                rotate: 0,
                stretch: 0,
                depth: 400,
                modifier: 1,
                slideShadows : true,
            }
        });

        $(".for-educators > .container").addClass("swiper-container");
        $(".for-educators .courses-row").addClass("swiper-wrapper");
        $(".for-educators .courses-row .col-md-3.col-xs-6").addClass("swiper-slide");
        let courseSwiper = new Swiper('.for-educators .swiper-container', {
            effect: 'coverflow',
            uniqueNavElements: true,
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 1.1,
            loop: true,
            spaceBetween: -10,
            coverflowEffect: {
                rotate: 0,
                stretch: 0,
                depth: 0,
                modifier: 0,
            }
        });

    }
    else{
        var swiper = new Swiper('.bg-image .swiper-container', {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 3,
            loop: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
              },
            coverflowEffect: {
                rotate: 0,
                stretch: 0,
                depth: 400,
                modifier: 1,
                slideShadows : true,
            }
        });
        var missionSwiper = new Swiper('.mission-images .swiper-container#mission-slider', {
            effect: 'coverflow',
            uniqueNavElements: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
              },
            spaceBetween: 85,
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 3,
            loop: true,
            coverflowEffect: {
                rotate: 0,
                stretch: 0,
                depth: 400,
                modifier: 1,
                slideShadows : true,    
            }
        });
    }
        


    $("header .search-div input.search-area").val("");
    $("main").click(function(){
        $("header .search-div input.search-area").css("width", "0");
        $("header .search-div input.search-area").css("padding", "0");
        $("header .search-div").css("right", "5%");
    });
    $("header .search-div .glyphicon-search").click(function(){
        if($(window).width() < 768){
            $("header .search-div input.search-area").css("width", "145px");   
        }
        else if($(window).width() < 361){
            $("header .search-div input.search-area").css("width", "120px");   
        }
        else{
            $("header .search-div input.search-area").css("width", "180px");
        }
        $("header .search-div input.search-area").css("padding", "6px 12px");
        $("header .search-div input.search-area").focus();
        $(".search-submit-btn").show();
        $("header .search-div").css("right", "1%");
    });
    $("header .toggle-button").click(function(){
        $("header .navbar-collapse.collapse .nav li").slideToggle();
        $("ul.wpm-language-switcher.switcher-list").show();
        $(".menu-container").addClass("overlay");
        $("header .navbar-collapse.collapse").addClass("overlay");
        $("header .search-div .glyphicon-search").hide();
        $("header .search-div input.search-area").css("width", "0");
        $("header .search-div input.search-area").css("padding", "0");
        $("header .search-div").css("right", "5%");
        $(".search-submit-btn").hide();
        $("main, footer").hide();
        $("header .close-button").show();
         $("header .toggle-button").hide();
    });
    $("header .close-button").click(function(){
        $("header .navbar-collapse.collapse .nav li").slideToggle();
        $("ul.wpm-language-switcher.switcher-list").hide();
         $(".menu-container").removeClass("overlay");
        $("header .navbar-collapse.collapse").removeClass("overlay");
        $("header .search-div .glyphicon-search").show();
        $(".search-submit-btn").hide();
        $("main, footer").show();
        $("header .close-button").hide();
        $("header .toggle-button").show();
    });
});
$(window).load(function() {
	var viewHeight = $(window).height();
	var bodyHeight = $("body").outerHeight();

	if(bodyHeight <= viewHeight) {
		$("body").css("min-height", viewHeight);
		$("footer").addClass("absolute-bottom");
	}
});
$(window).resize(function(){
    $("header .navbar-collapse.collapse").removeClass("overlay");
    $("main, footer").show();
    $(".menu-container").removeClass("overlay");
    $("header .search-div .glyphicon-search").show();
    $("header .close-button").hide();
    if($(window).width() > 991){
        $("header .navbar-collapse.collapse .nav li").show();
        $("header .toggle-button").hide();
    }
    else{
        $("header .navbar-collapse.collapse .nav li").hide();
        $("header .toggle-button").show();
    }
    if($(window).width() > 768){
        $(".for-educators > .container").removeClass("swiper-container");
        $(".for-educators .courses-row").removeClass("swiper-wrapper");
        $(".for-educators .courses-row .col-md-3.col-xs-6").removeClass("swiper-slide");

    }
    else{
        $(".for-educators > .container").addClass("swiper-container");
        $(".for-educators .courses-row").addClass("swiper-wrapper");
        $(".for-educators .courses-row .col-md-3.col-xs-6").addClass("swiper-slide");
        $("header .search-div input.search-area").css("width", "0");
        $("header .search-div input.search-area").css("padding", "0");
        $("header .search-div").css("right", "5%");
        $("header .navbar-collapse.collapse .nav li").hide();

    }

})

// UNCOMMENT FOLLOWING CODE IF YOU WANT TO PREVENT RIGHT CLICK OR INSPECT ELEMENT
// $(document).on("contextmenu", function() {
// 	return false; //Prevent right click
// });

// $(document).keydown(function(event) {
// 	if(event.keyCode == 123) {
// 		return false; //Prevent from f12
// 	} else if(event.ctrlKey && event.shiftKey && event.keyCode == 73) {
// 		return false; //Prevent from ctrl+shift+i
// 	}
// });