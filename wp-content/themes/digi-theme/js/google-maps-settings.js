// Create an array of styles.
var styles = [
    {
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "hue": "#970909"
            }
        ]
    }
];

// Create a new StyledMapType object, passing it the array of styles,
// as well as the name to be displayed on the map type control.
var styledMap = new google.maps.StyledMapType(styles, {
	name: "Styled Map"
});

// locations 
var map_location = new google.maps.LatLng(43.7700247, -79.3243468);

var icon = {
    // path:"M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z",
    // fillColor:"#ea4335",
    // fillOpacity:1,
    // anchor:new google.maps.Point(10.5,30),
    // strokeWeight:0,
    // scale:1.5
}

// Create a map object, and include the MapTypeId to add
// to the map type control.
var mapOptions = {
	zoom: 17,
	scrollwheel: false,
	center: map_location,
	mapTypeControlOptions: {
		mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
	}
};
var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
//Associate the styled map with the MapTypeId and set it to display.
map.mapTypes.set('map_style', styledMap);
map.setMapTypeId('map_style');

var beachMarker = new google.maps.Marker({
    position: map_location,
    map: map,
    // icon: icon,
    optimized: false
});

