<?php get_header(); ?>
<?php if (have_posts()) : $i=1; ?>
	<main class="news-page container">
		<h1 class="section-title text-center"><?php echo __('[:fr]Nouvelles et Evènements[:en]News And Events'); ?></h1>
		<div class="row news-row ">
        	<?php 
        	$args = array(
                'post_type'=>'post',
                'category_name' => 'news,events',
            );

            $the_query = new WP_Query( $args );
            the_post();
            while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

		    		<div class="col-md-offset-2 col-md-8 big-post">
		    			<div class="news-box">
							<a class="absolute" href="<?php the_permalink(); ?>"></a>
							<?php  if(has_post_thumbnail()): ?>
								<div class="event-thumbnail" style="background:url('<?php the_post_thumbnail_url("full"); ?>')  no-repeat center top / cover; "></div>
							<?php endif; ?>
							<div class="news-box-content">
								<div class="news-date">
									<p><b><?php echo get_the_date('j'); echo " "; echo get_the_date('M'); ?></b></p>
									<p><?php echo get_the_date('Y'); ?></p>
								</div>
								<div class="event-content">
									<h2 class="event-title"><?php the_title(); ?></h2>
									<p><?php echo my_excerpts(75); ?></p>
									<a class="read-more fr" href="<?php the_permalink(); ?>"><?php echo __('[:fr]Lire La Suite[:en]Read More'); ?> >></a>
								</div>	
							</div>
						</div>
					</div>
		    	
			<?php endwhile; endif; ?> 
		</div>
		<div class="nav-previous alignleft"><?php previous_posts_link( 'Newer Posts' ); ?></div>
		<div class="nav-next alignright"><?php next_posts_link( 'Older Posts' ); ?></div>
		<?php wp_reset_query(); wp_reset_postdata(); ?>
	</main>
<?php get_footer(); ?>