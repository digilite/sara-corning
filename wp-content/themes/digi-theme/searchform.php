<form action="<?php bloginfo("url"); ?>" method="get" id="search" class="search-div">
    <input  value="<?php (isset($_GET["s"]) ? _e($_GET["s"]) : ""); ?>" type="search" name="s" autocomplete="off" class="form-control search-area" placeholder="">
    <i class="glyphicon glyphicon-search"></i><button class="search-submit-btn" type="submit"></button>
</form>
				    