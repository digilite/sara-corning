<?php while(have_rows("social_links", "option")): the_row(); ?>
    <?php if(get_sub_field("link")): ?>
        <li>
            <a href="<?php the_sub_field("link", "option"); ?>" target="_blank" class="transition fa-icon">
                <?php the_sub_field("icon", "option"); ?>
            </a>
        </li>
    <?php endif; ?>
<?php endwhile; ?>

